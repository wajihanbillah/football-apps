package com.example.footballapps.data.model

import com.example.footballapps.domain.sports.model.Event
import com.google.gson.annotations.SerializedName

data class EventItem (
    @SerializedName(value = "idEvent") val idEvent: String?,
    @SerializedName(value = "idLeague") val idLeague: String?,
    @SerializedName(value = "strEvent") val strEvent: String?,
    @SerializedName(value = "strHomeTeam") val strHomeTeam: String?,
    @SerializedName(value = "strAwayTeam") val strAwayTeam: String?,
    @SerializedName(value = "intHomeScore") val intHomeScore: String?,
    @SerializedName(value = "intAwayScore") val intAwayScore: String?,
    @SerializedName(value = "dateEvent") val dateEvent: String?,
    @SerializedName(value = "strTime") val strTime: String?,
    @SerializedName(value = "idHomeTeam") val idHomeTeam: String?,
    @SerializedName(value = "idAwayTeam") val idAwayTeam: String?,
    @SerializedName(value = "strSport") val strSport: String?,
    @SerializedName(value = "strHomeGoalDetails") val strHomeGoalDetails: String?,
    @SerializedName(value = "strHomeRedCards") val strHomeRedCards: String?,
    @SerializedName(value = "strHomeYellowCards") val strHomeYellowCards: String?,
    @SerializedName(value = "strHomeFormation") val strHomeFormation: String?,
    @SerializedName(value = "strAwayGoalDetails") val strAwayGoalDetails: String?,
    @SerializedName(value = "strAwayRedCards") val strAwayRedCards: String?,
    @SerializedName(value = "strAwayYellowCards") val strAwayYellowCards: String?,
    @SerializedName(value = "strAwayFormation") val strAwayFormation: String?
){
    fun toEvent(): Event {
        return Event(
            idEvent = idEvent.orEmpty(),
            idLeague = idLeague.orEmpty(),
            strEvent = strEvent.orEmpty(),
            strHomeTeam = strHomeTeam.orEmpty(),
            strAwayTeam = strAwayTeam.orEmpty(),
            intHomeScore = intHomeScore.orEmpty(),
            intAwayScore = intAwayScore.orEmpty(),
            dateEvent = dateEvent.orEmpty(),
            strTime = strTime.orEmpty(),
            idHomeTeam = idHomeTeam.orEmpty(),
            idAwayTeam = idAwayTeam.orEmpty(),
            strSport = strSport.orEmpty(),
            strHomeGoalDetails = strHomeGoalDetails.orEmpty(),
            strHomeRedCards = strHomeRedCards.orEmpty(),
            strHomeYellowCards = strHomeYellowCards.orEmpty(),
            strHomeFormation = strHomeFormation.orEmpty(),
            strAwayGoalDetails = strAwayGoalDetails.orEmpty(),
            strAwayRedCards = strAwayRedCards.orEmpty(),
            strAwayYellowCards = strAwayYellowCards.orEmpty(),
            strAwayFormation = strAwayFormation.orEmpty()
        )
    }
}