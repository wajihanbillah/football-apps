package com.example.footballapps.data

import com.example.footballapps.data.model.EventItem
import com.example.footballapps.data.model.LeagueItem
import com.example.footballapps.data.model.TeamItem
import com.example.footballapps.utils.base.data.BaseRepository
import io.reactivex.Single

interface SportsRepository: BaseRepository {
    fun getLeagueList(leagueType: String): Single<List<LeagueItem>>

    fun getLeagueDetail(id: String): Single<List<LeagueItem>>

    fun getLastEvents(id: String): Single<List<EventItem>>

    fun getNextEvents(id: String): Single<List<EventItem>>

    fun getEvents(query: String): Single<List<EventItem>>

    fun getEventDetail(id: String): Single<List<EventItem>>

    fun getTeamDetail(id: String): Single<List<TeamItem>>
}