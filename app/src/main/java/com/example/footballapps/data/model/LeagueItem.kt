package com.example.footballapps.data.model

import com.example.footballapps.domain.sports.model.League
import com.google.gson.annotations.SerializedName

data class LeagueItem(
    @SerializedName(value = "idLeague") val idLeague: String?,
    @SerializedName(value = "strLeague") val strLeague: String?,
    @SerializedName(value = "strDescriptionEN") val strDescriptionEN: String?,
    @SerializedName(value = "intFormedYear") val intFormedYear: String?,
    @SerializedName(value = "strCountry") val strCountry: String?,
    @SerializedName(value = "strWebsite") val strWebsite: String?,
    @SerializedName(value = "strFacebook") val strFacebook: String?,
    @SerializedName(value = "strTwitter") val strTwitter: String?,
    @SerializedName(value = "strYoutube") val strYoutube: String?,
    @SerializedName(value = "strBadge") val strBadge: String?,
    @SerializedName(value = "strTrophy") val strTrophy: String?
) {
    fun toLeague(): League {
        return League(
            idLeague = idLeague.orEmpty(),
            strLeague = strLeague.orEmpty(),
            strDescriptionEN = strDescriptionEN.orEmpty(),
            intFormedYear = intFormedYear.orEmpty(),
            strCountry = strCountry.orEmpty(),
            strWebsite = strWebsite.orEmpty(),
            strFacebook = strFacebook.orEmpty(),
            strTwitter = strTwitter.orEmpty(),
            strYoutube = strYoutube.orEmpty(),
            strBadge = strBadge.orEmpty(),
            strTrophy = strTrophy.orEmpty()
        )
    }
}