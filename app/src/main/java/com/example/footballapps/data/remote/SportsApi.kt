package com.example.footballapps.data.remote

import com.example.footballapps.data.model.EventItem
import com.example.footballapps.data.model.LeagueItem
import com.example.footballapps.data.model.TeamItem
import com.example.footballapps.utils.base.data.ApiResponse
import com.example.footballapps.utils.base.data.WebApi
import io.reactivex.Single
import retrofit2.Response

class SportsApi(private val apiClient: SportsApiClient) : WebApi, SportsApiClient {
    override fun getLeagueList(leagueType: String): Single<Response<ApiResponse<List<LeagueItem>>>> {
        return apiClient.getLeagueList(leagueType)
    }

    override fun getLeagueDetail(id: String): Single<Response<ApiResponse<List<LeagueItem>>>> {
        return apiClient.getLeagueDetail(id)
    }

    override fun getLastEvents(id: String): Single<Response<ApiResponse<List<EventItem>>>> {
        return apiClient.getLastEvents(id)
    }

    override fun getNextEvents(id: String): Single<Response<ApiResponse<List<EventItem>>>> {
        return apiClient.getNextEvents(id)
    }

    override fun getEvents(query: String): Single<Response<ApiResponse<List<EventItem>>>> {
        return apiClient.getEvents(query)
    }

    override fun getEventDetail(id: String): Single<Response<ApiResponse<List<EventItem>>>> {
        return apiClient.getEventDetail(id)
    }

    override fun getTeamDetail(id: String): Single<Response<ApiResponse<List<TeamItem>>>> {
        return apiClient.getTeamDetail(id)
    }
}
