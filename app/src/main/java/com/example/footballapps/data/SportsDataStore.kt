package com.example.footballapps.data

import com.example.footballapps.data.model.EventItem
import com.example.footballapps.data.model.LeagueItem
import com.example.footballapps.data.model.TeamItem
import com.example.footballapps.data.remote.SportsApi
import com.example.footballapps.utils.base.data.erroroperator.singleApiError
import io.reactivex.Single

class SportsDataStore(api: SportsApi) : SportsRepository {

    override val dbService = null

    override val webService = api

    override fun getLeagueList(leagueType: String): Single<List<LeagueItem>> {
        return webService.getLeagueList(leagueType)
            .lift(singleApiError())
            .map { it.countrys }
    }

    override fun getLeagueDetail(id: String): Single<List<LeagueItem>> {
        return webService.getLeagueDetail(id)
            .lift(singleApiError())
            .map { it.leagues }
    }

    override fun getLastEvents(id: String): Single<List<EventItem>> {
        return webService.getLastEvents(id)
            .lift(singleApiError())
            .map { it.events }
    }

    override fun getNextEvents(id: String): Single<List<EventItem>> {
        return webService.getNextEvents(id)
            .lift(singleApiError())
            .map { it.events }
    }

    override fun getEvents(query: String): Single<List<EventItem>> {
        return webService.getEvents(query)
            .lift(singleApiError())
            .map { it.event }
    }

    override fun getEventDetail(id: String): Single<List<EventItem>> {
        return webService.getEventDetail(id)
            .lift(singleApiError())
            .map { it.events }
    }

    override fun getTeamDetail(id: String): Single<List<TeamItem>> {
        return webService.getTeamDetail(id)
            .lift(singleApiError())
            .map { it.teams }
    }
}