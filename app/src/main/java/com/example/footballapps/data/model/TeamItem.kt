package com.example.footballapps.data.model

import com.example.footballapps.domain.sports.model.Team
import com.google.gson.annotations.SerializedName

data class TeamItem(
    @SerializedName(value = "idTeam") val idTeam: String?,
    @SerializedName(value = "strTeamBadge") val strTeamBadge: String?
) {
    fun toTeam(): Team {
        return Team(
            idTeam = idTeam.orEmpty(),
            strTeamBadge = strTeamBadge.orEmpty()
        )
    }
}