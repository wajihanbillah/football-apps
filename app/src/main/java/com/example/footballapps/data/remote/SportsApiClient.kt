package com.example.footballapps.data.remote

import com.example.footballapps.data.model.EventItem
import com.example.footballapps.data.model.LeagueItem
import com.example.footballapps.data.model.TeamItem
import com.example.footballapps.utils.base.data.ApiResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SportsApiClient {
    @GET("search_all_leagues.php")
    fun getLeagueList(
        @Query("s") leagueType: String
    ): Single<Response<ApiResponse<List<LeagueItem>>>>

    @GET("lookupleague.php")
    fun getLeagueDetail(
        @Query("id") id: String
    ): Single<Response<ApiResponse<List<LeagueItem>>>>

    @GET("eventspastleague.php")
    fun getLastEvents(
        @Query("id") id: String
    ): Single<Response<ApiResponse<List<EventItem>>>>

    @GET("eventsnextleague.php")
    fun getNextEvents(
        @Query("id") id: String
    ): Single<Response<ApiResponse<List<EventItem>>>>

    @GET("searchevents.php")
    fun getEvents(
        @Query("e") query: String
    ): Single<Response<ApiResponse<List<EventItem>>>>

    @GET("lookupevent.php")
    fun getEventDetail(
        @Query("id") id: String
    ): Single<Response<ApiResponse<List<EventItem>>>>

    @GET("lookupteam.php")
    fun getTeamDetail(
        @Query("id") id: String
    ): Single<Response<ApiResponse<List<TeamItem>>>>
}