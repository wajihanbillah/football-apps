package com.example.footballapps.utils.base.data.erroroperator

import com.google.gson.Gson

fun <T> singleApiError(): SingleApiErrorOperator<T> {
    return SingleApiErrorOperator(Gson())
}