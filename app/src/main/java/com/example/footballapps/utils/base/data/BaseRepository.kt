package com.example.footballapps.utils.base.data

interface BaseRepository {
    val webService: WebApi?
    val dbService: LocalDb?
}