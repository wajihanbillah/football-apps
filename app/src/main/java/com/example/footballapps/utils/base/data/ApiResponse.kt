package com.example.footballapps.utils.base.data

import com.google.gson.annotations.SerializedName

class ApiResponse<T> {

    @SerializedName("data")
    val data: T? = null
    @SerializedName("countrys")
    val countrys: T? = null
    @SerializedName("leagues")
    val leagues: T? = null
    @SerializedName("events")
    val events: T? = null
    @SerializedName("teams")
    val teams: T? = null
    @SerializedName("event")
    val event: T? = null
}