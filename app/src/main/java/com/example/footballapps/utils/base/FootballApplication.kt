package com.example.footballapps.utils.base

import android.content.Context
import androidx.multidex.MultiDex
import com.example.footballapps.di.apiModule
import com.example.footballapps.di.featuremodules.sportsModule
import com.example.footballapps.di.rxModule
import com.example.footballapps.di.utilityModule
import org.koin.core.module.Module

class FootballApplication : BaseApplication() {

    override fun getDefinedModules(): List<Module> {
        return listOf(
            apiModule,
            utilityModule,
            sportsModule,
            rxModule
        )
    }


    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}