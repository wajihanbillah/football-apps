package com.example.footballapps.utils.base.data.erroroperator

import com.google.gson.Gson
import io.reactivex.SingleObserver
import io.reactivex.SingleOperator
import io.reactivex.disposables.Disposable
import retrofit2.Response

class SingleApiErrorOperator<T>(private val gson: Gson) : SingleOperator<T, Response<T>> {
    override fun apply(observer: SingleObserver<in T>): SingleObserver<in Response<T>> {
        return object : SingleObserver<Response<T>> {

            override fun onSuccess(response: Response<T>) {
                observer.onSuccess(response.body()!!)
            }

            override fun onSubscribe(d: Disposable) {
                observer.onSubscribe(d)
            }

            override fun onError(e: Throwable) {
                observer.onError(e)
            }
        }
    }
}