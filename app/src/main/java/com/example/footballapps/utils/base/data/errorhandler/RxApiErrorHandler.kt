package com.example.footballapps.utils.base.data.errorhandler

import androidx.lifecycle.MutableLiveData
import com.example.footballapps.utils.base.data.result.Result
import com.google.gson.JsonSyntaxException
import java.io.IOException
import java.lang.NullPointerException
import java.net.SocketTimeoutException


fun <T> genericErrorHandler(e: Throwable, result: MutableLiveData<Result<T>>) {
    when (e) {
        is SocketTimeoutException -> result.value = Result.fail(e, "Connection Timeout")
        is IOException -> result.value = Result.fail(e, "Connection IOException")
        is JsonSyntaxException -> result.value = Result.fail(e, "JSON Exception")
        is NullPointerException -> result.value = Result.empty()
        else -> result.value = Result.fail(e, "An unknown error occurred")
    }
}