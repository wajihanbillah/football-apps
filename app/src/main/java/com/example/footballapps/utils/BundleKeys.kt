package com.example.footballapps.utils

object BundleKeys {

    const val EVENT_ID = "eventId"

    const val EVENT_TYPE = "eventType"

    const val LEAGUE_ID = "leagueId"
}