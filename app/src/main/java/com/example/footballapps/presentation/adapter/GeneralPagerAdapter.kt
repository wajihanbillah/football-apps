package com.example.footballapps.presentation.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.footballapps.R
import com.example.footballapps.presentation.event.EventFragment
import com.example.footballapps.utils.enum.EventType

class GeneralPagerAdapter(fm: FragmentManager, val context: Context, idLeague: String) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val pages = listOf(
        EventFragment.newInstance(EventType.LAST_MATCH.type, idLeague),
        EventFragment.newInstance(EventType.NEXT_MATCH.type, idLeague)
    )

    override fun getItem(position: Int): Fragment = pages[position]

    override fun getCount(): Int = pages.size

    override fun getPageTitle(position: Int): CharSequence? = when (position) {
        0 -> context.getString(R.string.label_last_match)
        else -> context.getString(R.string.label_next_match)
    }
}