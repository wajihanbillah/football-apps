package com.example.footballapps.presentation.event

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.footballapps.R
import com.example.footballapps.domain.sports.model.Event
import com.example.footballapps.presentation.adapter.EventAdapter
import com.example.footballapps.presentation.eventdetail.EventDetailActivity
import com.example.footballapps.presentation.leaguedetail.LeagueDetailActivity
import com.example.footballapps.presentation.main.SportsViewModel
import com.example.footballapps.utils.BundleKeys
import com.example.footballapps.utils.base.data.result.Result
import com.example.footballapps.utils.emptyString
import com.example.footballapps.utils.enum.EventType
import com.example.footballapps.utils.enum.TeamType
import com.kennyc.view.MultiStateView
import kotlinx.android.synthetic.main.fragment_event.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class EventFragment : Fragment(), EventAdapter.OnEventListener,
    LeagueDetailActivity.OnLeagueDetailListener {

    private var eventType = emptyString()

    private lateinit var eventsAdapter: EventAdapter

    private var idLeague = emptyString()

    private val sportsViewModel: SportsViewModel by viewModel()

    private var listEvent = mutableListOf<Event>()

    private var listHomeId = mutableListOf<String>()

    private var listAwayId = mutableListOf<String>()

    private var position = 0

    companion object {
        fun newInstance(
            eventType: String = EventType.LAST_MATCH.type,
            idLeague: String
        ): EventFragment {
            val fragment = EventFragment()
            val bundle = Bundle()
            bundle.putString(BundleKeys.EVENT_TYPE, eventType)
            bundle.putString(BundleKeys.LEAGUE_ID, idLeague)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_event, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        LeagueDetailActivity.listener = this

        sportsViewModel.listEvent.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                    msvEvent.viewState = MultiStateView.ViewState.LOADING
                }
                is Result.Failure -> {
                    msvEvent.viewState = MultiStateView.ViewState.ERROR
                }
                is Result.Empty -> {
                    msvEvent.viewState = MultiStateView.ViewState.EMPTY
                }
                is Result.Success -> {
                    msvEvent.viewState = MultiStateView.ViewState.CONTENT

                    setEventDatas(it.data)
                }
            }
        })

        sportsViewModel.teamHome.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                }
                is Result.Failure -> {
                }
                is Result.Empty -> {
                }
                is Result.Success -> {

                    listEvent[position].strTeamBadgeHome = it.data[0].strTeamBadge

                    eventsAdapter.addOrUpdate(listEvent[position])

                    sportsViewModel.getTeamDetail(listAwayId[position], TeamType.AWAY.type)
                }
            }
        })

        sportsViewModel.teamAway.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                }
                is Result.Failure -> {
                }
                is Result.Empty -> {
                }
                is Result.Success -> {

                    listEvent[position].strTeamBadgeAway = it.data[0].strTeamBadge

                    eventsAdapter.addOrUpdate(listEvent[position])

                    if (position < listEvent.size - 1) {

                        position++

                        sportsViewModel.getTeamDetail(listHomeId[position], TeamType.HOME.type)
                    } else {
                        position = 0
                    }

                }
            }
        })

        arguments?.let {
            eventType = it.getString(BundleKeys.EVENT_TYPE) ?: emptyString()
            idLeague = it.getString(BundleKeys.LEAGUE_ID) ?: emptyString()
        }

        eventsAdapter = EventAdapter(listener = this)

        rvEvent.apply {
            layoutManager =
                LinearLayoutManager(
                    context,
                    LinearLayoutManager.VERTICAL,
                    false
                )
            setHasFixedSize(true)
            adapter = eventsAdapter
        }

        if (eventType == EventType.SEARCH.type)
            sportsViewModel.getListEvent(emptyString(), EventType.SEARCH.type)
        else
            sportsViewModel.getListEvent(idLeague, eventType)
    }

    private fun setEventDatas(datas: List<Event>) {

        listEvent.clear()
        listHomeId.clear()
        listAwayId.clear()

        listEvent = datas.toMutableList()

        val eventData = listEvent.filter { event ->
            event.strSport == getString(R.string.label_soccer_type) && event.idLeague == idLeague
        }

        if (eventType == EventType.SEARCH.type) {
            eventsAdapter.setEventData(eventData)
        } else {
            eventsAdapter.setEventData(listEvent)
        }

        listEvent.forEachIndexed { _, event ->
            listHomeId.add(event.idHomeTeam)
            listAwayId.add(event.idAwayTeam)
        }

        sportsViewModel.getTeamDetail(listHomeId[position], TeamType.HOME.type)
    }

    override fun onEventItemClicked(eventData: Event) {
        context?.let {
            EventDetailActivity.start(it, eventData.idEvent)
        }
    }

    override fun onSearchButtonCliced(query: String) {
        sportsViewModel.getListEvent(query, EventType.SEARCH.type)
    }
}
