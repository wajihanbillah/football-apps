package com.example.footballapps.presentation.main

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.example.footballapps.R
import com.example.footballapps.domain.sports.model.League
import com.example.footballapps.presentation.adapter.LeagueAdapter
import com.example.footballapps.presentation.leaguedetail.LeagueDetailActivity
import com.example.footballapps.presentation.search.SearchActivity
import com.example.footballapps.utils.base.data.result.Result
import com.example.footballapps.utils.setupToolbar
import com.kennyc.view.MultiStateView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity(), LeagueAdapter.OnLeagueListListener {

    private val sportsViewModel: SportsViewModel by viewModel()

    private lateinit var leagueAdapter: LeagueAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        sportsViewModel.listLeague.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                    msvLeagueList.viewState = MultiStateView.ViewState.LOADING

                    srLeague.isRefreshing = false
                }
                is Result.Failure -> {
                    msvLeagueList.viewState = MultiStateView.ViewState.ERROR

                    srLeague.isRefreshing = false
                }
                is Result.Empty -> {
                    msvLeagueList.viewState = MultiStateView.ViewState.EMPTY

                    srLeague.isRefreshing = false
                }
                is Result.Success -> {
                    msvLeagueList.viewState = MultiStateView.ViewState.CONTENT

                    srLeague.isRefreshing = false

                    leagueAdapter.setLeagueData(it.data)
                }
            }
        })

        setupToolbar(toolbar, getString(R.string.label_soccer_league))

        leagueAdapter = LeagueAdapter(listener = this)

        rvLeagueList.apply {
            layoutManager =
                GridLayoutManager(
                    this@MainActivity,
                    2,
                    GridLayoutManager.VERTICAL,
                    false
                )
            setHasFixedSize(true)
            adapter = leagueAdapter
        }

        srLeague.setOnRefreshListener {
            sportsViewModel.getListLeague(getString(R.string.label_soccer_type))
        }

        sportsViewModel.getListLeague(getString(R.string.label_soccer_type))
    }

    override fun onLeagueItemClicked(leagueData: League) {
        LeagueDetailActivity.start(this@MainActivity, leagueData.idLeague)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menuSearch -> {
                SearchActivity.start(this)
            }
        }
        return true
    }
}
