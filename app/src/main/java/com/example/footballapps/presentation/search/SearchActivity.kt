package com.example.footballapps.presentation.search

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.footballapps.R
import com.example.footballapps.domain.sports.model.Event
import com.example.footballapps.presentation.adapter.EventAdapter
import com.example.footballapps.presentation.eventdetail.EventDetailActivity
import com.example.footballapps.presentation.main.SportsViewModel
import com.example.footballapps.utils.base.data.result.Result
import com.example.footballapps.utils.enum.EventType
import com.example.footballapps.utils.enum.TeamType
import com.example.footballapps.utils.setupToolbar
import com.kennyc.view.MultiStateView
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class SearchActivity : AppCompatActivity(), EventAdapter.OnEventListener {

    companion object {
        fun start(context: Context) {
            context.startActivity<SearchActivity>()
        }
    }

    private lateinit var eventsAdapter: EventAdapter

    private val sportsViewModel: SportsViewModel by viewModel()

    private var listEvent = mutableListOf<Event>()

    private var listHomeId = mutableListOf<String>()

    private var listAwayId = mutableListOf<String>()

    private var position = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        setupToolbar(toolbar, getString(R.string.label_search_match), true)

        msvSearch.viewState = MultiStateView.ViewState.EMPTY

        sportsViewModel.listEvent.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                    msvSearch.viewState = MultiStateView.ViewState.LOADING
                }
                is Result.Failure -> {
                    msvSearch.viewState = MultiStateView.ViewState.ERROR
                }
                is Result.Empty -> {
                    msvSearch.viewState = MultiStateView.ViewState.EMPTY
                }
                is Result.Success -> {
                    msvSearch.viewState = MultiStateView.ViewState.CONTENT

                    setSearchDatas(it.data)
                }
            }
        })

        sportsViewModel.teamHome.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                }
                is Result.Failure -> {
                }
                is Result.Empty -> {
                }
                is Result.Success -> {

                    listEvent[position].strTeamBadgeHome = it.data[0].strTeamBadge

                    eventsAdapter.addOrUpdate(listEvent[position])

                    sportsViewModel.getTeamDetail(listAwayId[position], TeamType.AWAY.type)
                }
            }
        })

        sportsViewModel.teamAway.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                }
                is Result.Failure -> {
                }
                is Result.Empty -> {
                }
                is Result.Success -> {

                    listEvent[position].strTeamBadgeAway = it.data[0].strTeamBadge

                    eventsAdapter.addOrUpdate(listEvent[position])

                    if (position < listEvent.size - 1) {

                        position++

                        sportsViewModel.getTeamDetail(listHomeId[position], TeamType.HOME.type)
                    } else {
                        position = 0
                    }
                }
            }
        })

        eventsAdapter = EventAdapter(listener = this)

        rvSearch.apply {
            layoutManager =
                LinearLayoutManager(
                    context,
                    LinearLayoutManager.VERTICAL,
                    false
                )
            setHasFixedSize(true)
            adapter = eventsAdapter
        }
    }

    private fun setSearchDatas(datas: List<Event>) {

        val eventData = mutableListOf<Event>()

        listEvent.clear()
        listHomeId.clear()
        listAwayId.clear()

        listEvent = datas.toMutableList()

        listEvent.filterTo(eventData, {
            it.strSport.equals(getString(R.string.label_soccer_type), true)
        })

        eventsAdapter.setEventData(listEvent)

        listEvent.forEachIndexed { _, event ->
            listHomeId.add(event.idHomeTeam)
            listAwayId.add(event.idAwayTeam)
        }

        sportsViewModel.getTeamDetail(listHomeId[position], TeamType.HOME.type)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_search_view, menu)

        val searchItem = menu.findItem(R.id.menuSearchView)
        val searchView = searchItem.actionView as SearchView

        searchQuery(searchView)

        return true
    }

    private fun searchQuery(searchView: SearchView) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query.isNullOrEmpty()) {
                    return false
                }

                sportsViewModel.getListEvent(query, EventType.SEARCH.type)

                return true
            }
        }
        )
    }

    override fun onEventItemClicked(eventData: Event) {
        EventDetailActivity.start(this, eventData.idEvent)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return true
    }
}
