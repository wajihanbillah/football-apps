package com.example.footballapps.presentation.leaguedetail

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.example.footballapps.R
import com.example.footballapps.domain.sports.model.League
import com.example.footballapps.presentation.adapter.GeneralPagerAdapter
import com.example.footballapps.presentation.main.SportsViewModel
import com.example.footballapps.utils.BundleKeys
import com.example.footballapps.utils.base.data.result.Result
import com.example.footballapps.utils.emptyString
import com.example.footballapps.utils.setupToolbar
import com.kennyc.view.MultiStateView
import kotlinx.android.synthetic.main.activity_league_detail.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel


class LeagueDetailActivity : AppCompatActivity() {

    private val sportsViewModel: SportsViewModel by viewModel()

    private lateinit var league: League

    private var idLeague = emptyString()

    private var isSearch = false

    companion object {
        var listener: OnLeagueDetailListener? = null

        fun start(context: Context, leagueId: String) {
            context.startActivity<LeagueDetailActivity>(
                BundleKeys.LEAGUE_ID to leagueId
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_league_detail)

        sportsViewModel.league.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                    msvLeagueDetail.viewState = MultiStateView.ViewState.LOADING
                }
                is Result.Failure -> {
                    msvLeagueDetail.viewState = MultiStateView.ViewState.ERROR
                }
                is Result.Empty -> {
                    msvLeagueDetail.viewState = MultiStateView.ViewState.EMPTY
                }
                is Result.Success -> {
                    msvLeagueDetail.viewState = MultiStateView.ViewState.CONTENT

                    league = it.data[0]

                    showLeagueDetail(it.data)
                }
            }
        })

        idLeague = intent.getStringExtra(BundleKeys.LEAGUE_ID) ?: emptyString()

        setupToolbar(toolbar, getString(R.string.label_league_detail), true)

        initViewPagerEvents()

        initButton()

        sportsViewModel.getLeagueDetail(idLeague)
    }

    private fun initViewPagerEvents() {
        vpLeagueDetail.adapter =
            GeneralPagerAdapter(supportFragmentManager, this, idLeague = idLeague)
        vpLeagueDetail.offscreenPageLimit = 2
        tabLeagueDetail.setupWithViewPager(vpLeagueDetail)
        vpLeagueDetail.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }
            override fun onPageSelected(position: Int) {
                if (position == 2) {
                    isSearch = true

                    invalidateOptionsMenu()

//                    appbarLeagueDetail.visibility = View.GONE
//
//                    val params = vpLeagueDetail.layoutParams as CoordinatorLayout.LayoutParams
//                    params.behavior = null
//                    vpLeagueDetail.requestLayout()
                } else {
                    isSearch = false

                    invalidateOptionsMenu()
                }
            }

        })
    }

    private fun initButton() {
        btnMore.setOnClickListener {
            if (btnMore.text == getString(R.string.action_more)) {
                btnMore.text = getString(R.string.action_hide)

                tvDescription.maxLines = Int.MAX_VALUE
            } else {
                btnMore.text = getString(R.string.action_more)

                tvDescription.maxLines = 5
            }
        }

        btnWebsite.setOnClickListener {
            openWebPage(league.strWebsite)
        }

        btnFacebook.setOnClickListener {
            openWebPage(league.strFacebook)
        }

        btnTwitter.setOnClickListener {
            openWebPage(league.strTwitter)
        }

        btnYoutube.setOnClickListener {
            openWebPage(league.strYoutube)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_search_view, menu)

        val searchItem = menu.findItem(R.id.menuSearchView)

        if (isSearch) {
            searchItem.isVisible = true

            val searchView = searchItem.actionView as SearchView

            searchQuery(searchView)

        } else {
            searchItem.isVisible = false
        }

        return true
    }

    private fun searchQuery(searchView: SearchView) {
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query.isNullOrEmpty()) {
                    return false
                }

                listener?.onSearchButtonCliced(query)

                return true
            }
        }
        )
    }

    private fun showLeagueDetail(data: List<League>) {
        tvTitle.text = data[0].strLeague
        tvCountry.text = String.format(getString(R.string.format_country), data[0].strCountry)
        tvYearFormed.text =
            String.format(getString(R.string.format_year_formed), data[0].intFormedYear)
        tvDescription.text = data[0].strDescriptionEN

        Glide.with(this)
            .load(data[0].strTrophy)
            .into(imgTrophy)
    }

    private fun openWebPage(url: String) {
        val link = String.format(getString(R.string.format_url), url)
        val webpage: Uri = Uri.parse(link)
        val intent = Intent(Intent.ACTION_VIEW, webpage)
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

    interface OnLeagueDetailListener {
        fun onSearchButtonCliced(query: String)
    }
}
