package com.example.footballapps.presentation.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.footballapps.domain.sports.SportsUseCase
import com.example.footballapps.domain.sports.model.Event
import com.example.footballapps.domain.sports.model.League
import com.example.footballapps.domain.sports.model.Team
import com.example.footballapps.utils.base.data.errorhandler.genericErrorHandler
import com.example.footballapps.utils.base.data.result.Result
import com.example.footballapps.utils.base.data.schedulertransformer.singleScheduler
import com.example.footballapps.utils.enum.EventType
import com.example.footballapps.utils.enum.TeamType
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

class SportsViewModel(
    private val sportsUseCase: SportsUseCase,
    private val disposable: CompositeDisposable
) : ViewModel() {

    val listLeague = MutableLiveData<Result<List<League>>>()
    val league = MutableLiveData<Result<List<League>>>()
    val listEvent = MutableLiveData<Result<List<Event>>>()
    val event = MutableLiveData<Result<List<Event>>>()
    val teamHome = MutableLiveData<Result<List<Team>>>()
    val teamAway = MutableLiveData<Result<List<Team>>>()

    init {
        listLeague.value = Result.default()
        league.value = Result.default()
        listEvent.value = Result.default()
        event.value = Result.default()
        teamHome.value = Result.default()
        teamAway.value = Result.default()
    }

    fun getListLeague(leagueType: String) {
        listLeague.value = Result.loading()
        sportsUseCase.getLeagueList(leagueType = leagueType)
            .compose(singleScheduler())
            .subscribe({
                if (it.isNullOrEmpty()) listLeague.value = Result.empty()
                else listLeague.value = Result.success(it)
            }, {
                genericErrorHandler(it, listLeague)
            })
            .addTo(disposable)
    }

    fun getLeagueDetail(id: String) {
        league.value = Result.loading()
        sportsUseCase.getLeagueDetail(id = id)
            .compose(singleScheduler())
            .subscribe({
                if (it.isNullOrEmpty()) league.value = Result.empty()
                else league.value = Result.success(it)
            }, { genericErrorHandler(it, league) })
            .addTo(disposable)
    }

    fun getListEvent(string: String, type: String) {

        listEvent.value = Result.loading()

        when (type) {
            EventType.LAST_MATCH.type -> {
                sportsUseCase.getLastEvents(id = string)
                    .compose(singleScheduler())
                    .subscribe({
                        if (it.isNullOrEmpty()) listEvent.value = Result.empty()
                        else listEvent.value = Result.success(it)
                    }, {
                        genericErrorHandler(it, listEvent)
                    })
                    .addTo(disposable)
            }
            EventType.NEXT_MATCH.type -> {
                sportsUseCase.getNextEvents(id = string)
                    .compose(singleScheduler())
                    .subscribe({
                        if (it.isNullOrEmpty()) listEvent.value = Result.empty()
                        else listEvent.value = Result.success(it)
                    }, {
                        if (it == null) listEvent.value = Result.empty()
                        else genericErrorHandler(it, listEvent)
                    })
                    .addTo(disposable)
            }
            EventType.SEARCH.type -> {
                sportsUseCase.getEvents(query = string)
                    .compose(singleScheduler())
                    .subscribe({
                        if (it.isNullOrEmpty()) listEvent.value = Result.empty()
                        else listEvent.value = Result.success(it)
                    }, {
                        genericErrorHandler(it, listEvent)
                    })
                    .addTo(disposable)
            }
        }
    }

    fun getEventDetail(id: String) {
        event.value = Result.loading()
        sportsUseCase.getEventDetail(id = id)
            .compose(singleScheduler())
            .subscribe({
                if (it.isNullOrEmpty()) event.value = Result.empty()
                else event.value = Result.success(it)
            }, {
                genericErrorHandler(it, event)
            })
            .addTo(disposable)
    }

    fun getTeamDetail(id: String, type: String
//                      ,position: Int
    ) {
        if (type == TeamType.HOME.type) {
            teamHome.value = Result.loading()
            sportsUseCase.getTeamDetail(id = id)
                .compose(singleScheduler())
                .subscribe({
                    if (it.isNullOrEmpty()) teamHome.value = Result.empty()
                    else teamHome.value = Result.success(it)
                }, {
                    genericErrorHandler(it, teamHome)
                })
                .addTo(disposable)
        } else {
            teamAway.value = Result.loading()
            sportsUseCase.getTeamDetail(id = id)
                .compose(singleScheduler())
                .subscribe({
                    if (it.isNullOrEmpty()) teamAway.value = Result.empty()
                    else teamAway.value = Result.success(it)
                }, {
                    genericErrorHandler(it, teamAway)
                })
                .addTo(disposable)
        }
    }
}