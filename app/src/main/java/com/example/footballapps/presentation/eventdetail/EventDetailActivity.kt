package com.example.footballapps.presentation.eventdetail

import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.footballapps.R
import com.example.footballapps.domain.sports.model.Event
import com.example.footballapps.domain.sports.model.EventDetail
import com.example.footballapps.presentation.adapter.EventDetailAdapter
import com.example.footballapps.presentation.main.SportsViewModel
import com.example.footballapps.utils.BundleKeys
import com.example.footballapps.utils.base.data.result.Result
import com.example.footballapps.utils.dateConverter
import com.example.footballapps.utils.emptyString
import com.example.footballapps.utils.enum.TeamType
import com.example.footballapps.utils.setupToolbar
import com.kennyc.view.MultiStateView
import kotlinx.android.synthetic.main.activity_event_detail.*
import kotlinx.android.synthetic.main.fragment_event.*
import kotlinx.android.synthetic.main.item_event.view.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import org.jetbrains.anko.startActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class EventDetailActivity : AppCompatActivity() {

    private var eventId = emptyString()

    private val sportsViewModel: SportsViewModel by viewModel()

    private lateinit var eventDetailAdapter: EventDetailAdapter


    companion object {
        fun start(context: Context, eventId: String) {
            context.startActivity<EventDetailActivity>(
                BundleKeys.EVENT_ID to eventId
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_event_detail)

        sportsViewModel.event.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
                    msvEventDetail.viewState = MultiStateView.ViewState.LOADING
                }
                is Result.Failure -> {
                    msvEventDetail.viewState = MultiStateView.ViewState.ERROR
                }
                is Result.Empty -> {
                    msvEventDetail.viewState = MultiStateView.ViewState.EMPTY
                }
                is Result.Success -> {
//                    msvEventDetail.viewState = MultiStateView.ViewState.CONTENT

                    showEventDetail(it.data)
                }
            }
        })

        sportsViewModel.teamHome.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
//                    msvEvent.viewState = MultiStateView.ViewState.LOADING
                }
                is Result.Failure -> {
//                    msvEvent.viewState = MultiStateView.ViewState.ERROR
                }
                is Result.Empty -> {
//                    msvEvent.viewState = MultiStateView.ViewState.EMPTY
                }
                is Result.Success -> {
//                    msvEvent.viewState = MultiStateView.ViewState.CONTENT

                    Glide.with(this)
                        .load(it.data[0].strTeamBadge)
                        .into(imgBadgeHome)
                }
            }
        })

        sportsViewModel.teamAway.observe(this, Observer {
            when (it) {
                is Result.Loading -> {
//                    msvEvent.viewState = MultiStateView.ViewState.LOADING
                }
                is Result.Failure -> {
//                    msvEvent.viewState = MultiStateView.ViewState.ERROR
                }
                is Result.Empty -> {
//                    msvEvent.viewState = MultiStateView.ViewState.EMPTY
                }
                is Result.Success -> {
                    msvEventDetail.viewState = MultiStateView.ViewState.CONTENT

                    Glide.with(this)
                        .load(it.data[0].strTeamBadge)
                        .into(imgBadgeAway)
                }
            }
        })

        eventId = intent.getStringExtra(BundleKeys.EVENT_ID) ?: emptyString()

        setupToolbar(toolbar, getString(R.string.label_event_detail), true)

        eventDetailAdapter = EventDetailAdapter(mutableListOf())

        rvEventDetail.apply {
            layoutManager =
                LinearLayoutManager(
                    context,
                    LinearLayoutManager.VERTICAL,
                    false
                )
            setHasFixedSize(true)
            adapter = eventDetailAdapter
        }

        sportsViewModel.getEventDetail(eventId)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return true
    }

    private fun showEventDetail(data: List<Event>) {
        tvEventName.text = data[0].strEvent
        tvEventDateTime.text =
            dateConverter(this, data[0].dateEvent, data[0].strTime)
        tvTeamHome.text = data[0].strHomeTeam
        tvTeamAway.text = data[0].strAwayTeam

        val homeScore =
            if (data[0].intHomeScore.isNotEmpty()) data[0].intHomeScore else getString(
                R.string.label_minus
            )

        val awayScore =
            if (data[0].intAwayScore.isNotEmpty()) data[0].intAwayScore else getString(
                R.string.label_minus
            )

        tvScore.text =
            String.format(getString(R.string.format_score), homeScore, awayScore)


        val listOfHome = listOf(data[0].strHomeGoalDetails, data[0].strHomeRedCards, data[0].strHomeYellowCards, data[0].strHomeFormation)
        val listOfCategory = resources.getStringArray(R.array.list_of_event_detail_category)
        val listOfAway = listOf(data[0].strAwayGoalDetails, data[0].strAwayRedCards, data[0].strAwayYellowCards, data[0].strAwayFormation)

        val eventDetail = mutableListOf<EventDetail>()

        listOfHome.forEachIndexed { i, _ ->
            eventDetail.add(
                EventDetail(
                    listOfHome[i],
                    listOfCategory[i],
                    listOfAway[i]
                )
            )
        }

        eventDetailAdapter.setEventDetailData(eventDetail)

        sportsViewModel.getTeamDetail(data[0].idHomeTeam, TeamType.HOME.type
//            , 0
        )

        sportsViewModel.getTeamDetail(data[0].idAwayTeam, TeamType.AWAY.type
//            , 0
        )
    }
}
