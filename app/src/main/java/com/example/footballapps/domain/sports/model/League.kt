package com.example.footballapps.domain.sports.model

data class League (
    val idLeague: String,
    val strLeague: String,
    val strDescriptionEN: String,
    val intFormedYear: String,
    val strCountry: String,
    val strWebsite: String,
    val strFacebook: String,
    val strTwitter: String,
    val strYoutube: String,
    val strBadge: String,
    val strTrophy: String
)