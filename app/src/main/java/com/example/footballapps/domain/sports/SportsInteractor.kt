package com.example.footballapps.domain.sports

import com.example.footballapps.data.SportsRepository
import com.example.footballapps.domain.sports.model.Event
import com.example.footballapps.domain.sports.model.League
import com.example.footballapps.domain.sports.model.Team
import io.reactivex.Single

class SportsInteractor(private val repository: SportsRepository) : SportsUseCase {

    override fun getLeagueList(leagueType: String): Single<List<League>> {
        return repository.getLeagueList(leagueType)
            .map { items ->
                items.map {
                    it.toLeague()
                }
            }
    }

    override fun getLeagueDetail(id: String): Single<List<League>> {
        return repository.getLeagueDetail(id)
            .map { items ->
                items.map {
                    it.toLeague()
                }
            }
    }

    override fun getLastEvents(id: String): Single<List<Event>> {
        return repository.getLastEvents(id)
            .map { items ->
                items.map {
                    it.toEvent()
                }
            }
    }

    override fun getNextEvents(id: String): Single<List<Event>> {
        return repository.getNextEvents(id)
            .map { items ->
                items.map {
                    it.toEvent()
                }
            }
    }

    override fun getEvents(query: String): Single<List<Event>> {
        return repository.getEvents(query)
            .map { items ->
                items.map {
                    it.toEvent()
                }
            }
    }

    override fun getEventDetail(id: String): Single<List<Event>> {
        return repository.getEventDetail(id)
            .map { items ->
                items.map {
                    it.toEvent()
                }
            }
    }

    override fun getTeamDetail(id: String): Single<List<Team>> {
        return repository.getTeamDetail(id)
            .map { items ->
                items.map {
                    it.toTeam()
                }
            }
    }
}