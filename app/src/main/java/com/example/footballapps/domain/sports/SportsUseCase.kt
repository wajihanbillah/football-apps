package com.example.footballapps.domain.sports

import com.example.footballapps.domain.sports.model.Event
import com.example.footballapps.domain.sports.model.EventExpanded
import com.example.footballapps.domain.sports.model.League
import com.example.footballapps.domain.sports.model.Team
import io.reactivex.Single

interface SportsUseCase {
    fun getLeagueList(leagueType: String): Single<List<League>>

    fun getLeagueDetail(id: String): Single<List<League>>

    fun getLastEvents(id: String): Single<List<Event>>

    fun getNextEvents(id: String): Single<List<Event>>

    fun getEvents(query: String): Single<List<Event>>

    fun getEventDetail(id: String): Single<List<Event>>

    fun getTeamDetail(id: String): Single<List<Team>>
}