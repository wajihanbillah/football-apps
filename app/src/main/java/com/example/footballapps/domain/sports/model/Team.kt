package com.example.footballapps.domain.sports.model

data class Team (
    val idTeam: String,
    val strTeamBadge: String
)