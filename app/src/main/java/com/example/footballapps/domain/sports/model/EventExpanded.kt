package com.example.footballapps.domain.sports.model

import io.reactivex.Single

data class EventExpanded(
    val event: Single<List<Event>>,
    val teamHome: Single<List<Team>>,
    val teamAway: Single<List<Team>>
)