package com.example.footballapps.domain.sports.model

class EventDetail (
    val home: String,
    val category: String,
    val away: String
)