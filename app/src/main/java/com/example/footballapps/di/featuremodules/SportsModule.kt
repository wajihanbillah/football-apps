package com.example.footballapps.di.featuremodules

import com.example.footballapps.data.SportsDataStore
import com.example.footballapps.data.SportsRepository
import com.example.footballapps.data.remote.SportsApi
import com.example.footballapps.data.remote.SportsApiClient
import com.example.footballapps.di.BASE_URL
import com.example.footballapps.domain.sports.SportsInteractor
import com.example.footballapps.domain.sports.SportsUseCase
import com.example.footballapps.presentation.adapter.EventAdapter
import com.example.footballapps.presentation.main.SportsViewModel
import com.example.footballapps.utils.base.data.ApiService
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

val sportsModule = module {

    single {
        ApiService.createReactiveService(
            SportsApiClient::class.java,
            get(),
            get(named(BASE_URL))
        )
    }

    single { SportsApi(get()) }

    single<SportsRepository> { SportsDataStore(get()) }

    single<SportsUseCase> { SportsInteractor(get()) }

    viewModel { SportsViewModel(get(), get()) }
}