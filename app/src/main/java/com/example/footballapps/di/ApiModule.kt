package com.example.footballapps.di

import com.example.footballapps.BuildConfig
import com.example.footballapps.utils.base.data.okhttp.OkHttpClientFactory
import org.koin.core.qualifier.named
import org.koin.dsl.module

const val BASE_URL = "base_url"

val apiModule = module {

    single {
        return@single OkHttpClientFactory.create(
            showDebugLog = BuildConfig.DEBUG
        )
    }

    single(named(BASE_URL)) { BuildConfig.BASE_URL }
}
